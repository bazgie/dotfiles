#!/bin/bash
mkdir -p ~/apps/openocd
cd ~/apps/openocd
git clone git://git.code.sf.net/p/openocd/code && cd code
sudo apt-get install git zlib1g-dev libtool flex bison libgmp3-dev  libmpfr-dev libncurses5-dev libmpc-dev autoconf texinfo build-essential  libftdi-dev libusb-1.0.0-dev
./bootstrap && ./configure && make
sudo make install
