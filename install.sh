#!/bin/bash

#check if sourced
if [[ $_ == $0 ]]; then
  echo -e "Please source this script as:\nsource ${0##*/}"
  exit 0
fi

SUDO=''
#root priviliges
while true; do
  read -p "Run as sudo? [y/n]" yn
  case $yn in
    [Yy]* )
      SUDO='sudo'
      break;;
    [Nn]* )
      break;;
    * ) echo "Please answer yes or no [Yy|Nn]";;
  esac
done

#keys ssh create
while true; do
  read -p "Generate ssh key? [y/n]" yn
  case $yn in
    [Yy]* ) 
      ssh-keygen
      break;;
    [Nn]* ) 
      break;;
    * ) echo "Please answer yes or no.";;
  esac
done

#check if root
if [ "$SUDO" = 'sudo' ]; then
  #has root priviliges
  while true; do
    read -p "Install packages as root? [y/n]" yn
    case $yn in
      [Yy]* ) 
        sudo apt update && \ apt upgrade
        #cmake
        sudo apt install software-properties-common
        sudo add-apt-repository ppa:george-edison55/cmake-3.x
        sudo apt install cmake
        #python3
        apt install python3
        apt install python3-pip
        apt install tree
        apt install htop
        apt install tcpdump
        break;;
      [Nn]* ) 
        break;;
      * ) echo "Please answer yes or no.";;
    esac
  done
fi

#NEOVIM
while true; do
  read -p "Install neovim? [y/n]" yn
  case $yn in
    [Yy]* ) 
      ./install_nvim.sh
      break;;
    [Nn]* ) 
      break;;
    * ) echo "Please answer yes or no.";;
  esac
done

#KEYCHAIN
while true; do
  read -p "Install keychain? [y/n]" yn
  case $yn in
    [Yy]* )
      apt source keychain
      rm -rf ~/apps/keychain
      mkdir ~/apps/keychain
      mv keychain* ~/apps/keychain/
      ln -s ~/apps/keychain/keychain-2.8.2/keychain ~/bin/keychain
      break;;
    [Nn]* )
      break;;
    * ) echo "Please answer yes or no.";;
  esac
done

#Links
while true; do
  read -p "Make links? [y/n]" yn
  case $yn in
    [Yy]* )
      source ~/dotfiles/install_links.sh
      break;;
    [Nn]* )
      break;;
    * ) echo "Please answer yes or no.";;
  esac
done

#reload
. ~/.bashrc
. ~/.profile
