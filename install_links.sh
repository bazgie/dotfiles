#!/bin/bash


if [[ $_ == $0 ]]; then
  echo -e "Please source this script as:\nsource ${0##*/}"
  exit
fi


#make links
rm -f ~/.bashrc
ln -s ~/dotfiles/.bashrc ~/.bashrc
ln -s ~/dotfiles/.bash_colors ~/.bash_colors
rm -f ~/.profile
ln -s ~/dotfiles/.profile ~/.profile
ln -s ~/dotfiles/.bash_aliases ~/.bash_aliases
ln -s ~/dotfiles/.tmux.conf ~/.tmux.conf
ln -s ~/dotfiles/.cgdb ~/.cgdb
ln -s ~/dotfiles/.merger.sh ~/.merger.sh
ln -s ~/dotfiles/.diffwrap.sh ~/.diffwrap.sh
ln -s ~/dotfiles/.vifm ~/.vifm
ln -s ~/dotfiles/.gitconfig ~/.gitconfig

#other
mkdir -p ~/projects
