#!/bin/bash


#pip3 must be installed
{
  pip3 install neovim
} || {
  echo -e "\n\n **** Error while using pip3"
  exit 1
}

#neovim
rm ~/.vimrc
rm -rf ~/apps/neovim
rm -rf ~/bin/nvim
rm -rf ~/bin/vim

mkdir -p ~/apps/neovim
curl -fLo ~/apps/neovim/nvim.appimage https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage
chmod u+x ~/apps/neovim/nvim.appimage
(cd ~/apps/neovim/; ./nvim.appimage --appimage-extract)
mkdir -p ~/.config/nvim
cp ~/dotfiles/init.vim  ~/.config/nvim/init.vim
mkdir -p ~/.vim/colors
cp ~/dotfiles/.vim/colors/lucius.vim  ~/.vim/colors/lucius.vim
mkdir -p ~/bin/
ln -s ~/dotfiles/.vimrc ~/.vimrc
ln -s ~/apps/neovim/squashfs-root/AppRun ~/bin/nvim
ln -s ~/apps/neovim/squashfs-root/AppRun ~/bin/vim

#neovim plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

~/bin/./vim -c 'PlugInstall' -c 'q'
~/bin/./vim -c 'UpdateRemotePlugins' -c 'q'
~/bin/./vim -c 'q'

#bitbake syntax
echo -e `git clone https://github.com/kergoth/vim-bitbake.git /tmp/vim-bitbake/`
echo -e `mv /tmp/vim-bitbake/ftdetect/ ~/apps/neovim/squashfs-root/usr/share/nvim/runtime/`
echo -e `mv /tmp/vim-bitbake/ftplugin/bitbake.vim ~/apps/neovim/squashfs-root/usr/share/nvim/runtime/ftplugin/`
echo -e `mv /tmp/vim-bitbake/indent/bitbake.vim ~/apps/neovim/squashfs-root/usr/share/nvim/runtime/indent/`
echo -e `mv /tmp/vim-bitbake/plugin/newbb* ~/apps/neovim/squashfs-root/usr/share/nvim/runtime/plugin/`
echo -e `mv /tmp/vim-bitbake/syntax/bitbake.vim ~/apps/neovim/squashfs-root/usr/share/nvim/runtime/syntax/`
echo -e `rm -rf /tmp/vim-bitbake/`


# fzf
rm -rf ~/.fzf/
rm -rf ~/.tmux/plugins/tpm
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
~/.fzf/install
