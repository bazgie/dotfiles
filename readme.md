Setup in steps:

--> 0. Clone repo into home directory (use https, not ssh)
git clone https://bazgie@bitbucket.org/bazgie/dotfiles.git
----------------------------------------------------------

--> 1. Firstly run:
source ~/dotfiles/install.sh
check if colors changed (and bashrc changed)

--> 2. Check git user name and mail in ~/.gitconfig

--> 3. Optionally change machine colors via (PS_MACHINE_COLOR):
vim ~/.bashrc

